# SurfEP

## Overview
- Released by the [Montemore group at Tulane University](https://www.montemoregroup.org/). Email mmontemore@tulane.edu with questions or comments.
- SurfEP, for Surface Energetics Prediction, allows the prediction of adsorption energies on metal alloy surfaces, as well as surface formation energies. These are currently implemented separately.
- If you use this package or method in your research, please cite the relevant publication(s).

## Instructions for adsorption energy prediction using SurfEP
[Paper here](https://pubs.rsc.org/en/content/articlelanding/2020/cy/d0cy00682c)

SurfEP is organized as a class, which is initialized by calling surfEP(). There are optional arguments for the locations of the files that give the model parameters. Once a class object has been created, the primary function is atomsToAds(), which takes a list of atoms objects for the surfaces, a list of adsorbates, the site type, a list of the site indices (a list of lists, where each sublist contains the indices of a site), and a list of surface indices (the indices of all of the atoms in the surface). The Jupyter Notebook SurfEPExamples.ipynb gives examples for how to use this function.

- Possible host metals: ['Cu','Ag','Au','Ni','Pt','Pd','Co','Rh','Ir','Ru','Os','Re','Ti','Zr','Hf','Sc']
- Possible dopant metals: ['Cu','Ag','Au','Ni','Pt','Pd','Co','Rh','Ir','Fe','Ru','Os','Mn','Re','Cr','Mo','W','V','Ta','Ti','Zr','Hf','Sc']
- Possible adsorbates: ['C', 'N', 'O', 'OH', 'H', 'S', 'K', 'F']
- Possible siteTypes: ['Top','Bridge','Hollow'] 
	- Not all siteTypes may be available for all species.
- Other limitations: Currently, only bimetallic fcc(111) and hcp(0001) surfaces are supported, and each surface must be a pure metal in the bulk but can be doped with other atoms in the top two layers. Other geometries and architectures may run, but the predictions are unlikely to be reliable. Further, if the surface dramatically reconstructs or the adsorbate relaxes out of site, the predictions are unlikely to be reliable. The surfaceIndicesList must be sequential and start at 0 (hence, the first few atoms in the atoms object must be in the top layer of the slab).

The current version of this package is an early release. While it has been tested, it may have unexpected behavior in some situations.

If comparing to your own DFT data, we suggest you do a linear fit between SurfEP predictions and your data, for the particular subset of alloys and adsorbate you're interested in.

## Instructions for surface energy prediction
[Paper here](https://iopscience.iop.org/article/10.1088/2515-7655/aca122)

- See SurfaceStability_ECFP_Example.ipynb.
- The possible surfaces are the same as those for the adsorption energy prediction.

## Instructions for adsorption energy prediction using Latent-variable SurfEP
[Paper here]()

Latent-variable SurfEP (Lv_surfep) is an improved version of SurfEP that uses latent variables in its prediction task. It works similarly to SurfEP in that it returns predictions once given a list of atoms objects for the surfaces, a list of adsorbates, the site type, a list of the site indices (a list of lists, where each sublist contains the indices of a site), and a list of surface indices (the indices of all of the atoms in the surface). It can also return the latent variables themselves which can further be used for transfer learning or new feature representations. All of these are demonstrated in the Jupyter Notebook LatentVariableSurfEPExamples.ipynb.

The version of Lv_surfep implemented here was trained using adsorption energies from the mamun et al dataset. Computational details can be found in their [paper](https://www.nature.com/articles/s41597-019-0080-z#Sec2).

To be consistent with SurfEP, we returned the negative predictions i.e np.negative(prediction). This is because mamun et al's dataset was calculated using different a different adsorption reference.

-Possibe host and guest metals are same as SurfEP above 
- Possible adsorbates: [H, N, C, O, CH3, CH2, CH, and NH]
- Possible siteTypes: ['Top','Bridge','FCCHollow','HCPHollow'] 

- Other limitations: Similar to SurfEP, only bimetallic fcc(111) and hcp(0001) surfaces are supported, and each surface must be a pure metal in the bulk but can be doped with other atoms in the top two layers. Other geometries and architectures may run, but the predictions are unlikely to be reliable. Further, if the surface dramatically reconstructs or the adsorbate relaxes out of site, the predictions are unlikely to be reliable. The surfaceIndicesList must be sequential and start at 0 (hence, the first few atoms in the atoms object must be in the top layer of the slab).

The current version of this package is an early release. While it has been tested, it may have unexpected behavior in some situations.

If comparing to your own DFT data, we suggest you do a linear fit between SurfEP predictions and your data, for the particular subset of alloys and adsorbate you're interested in.
